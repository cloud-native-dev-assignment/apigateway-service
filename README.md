# API Gateway Service

API Gateway Service is API gateway for Freelancer Service and Project Service. API Gateway Service use following main technology
- Thorntail
- Micro Profile: CDI, REST Client, Fault Tolerance, Health
- Mockito
- Fabric8
- ConfigMap

## Test with Arquillian

This service used Arquillian for testing and used Mockito for mock backend services

```
mvn test
```

## Running with Local Profile

This service can be run locally on your machine by following command

```
java -jar target/apigateway-service-thorntail.jar -Slocal
```

Local profile used following backend services

- http://localhost:8081/freelancers for Freelance Service
- http://localhost:8082/projects for Project Service

YAML file for local profile located at src/main/java/resources/project-local.yml


## Deploy to OpenShift

Fabric8 plugin can be used to deploy to OpenShfit with S2I by
```
mvn fabric8:deploy -Popenshift
```

### API document

Following URI are supported

| Method | URI                                   |  Description                |
|--------|---------------------------------------|-----------------------------
| GET    | /gateway/freelancers                  |  get all freelancers        |
| GET    | /gateway/freelancers/{freelancerId}   |  get freelancer by ID       | 
| GET    | /gateway/projects                     |  get all projects           |
| POST   | /gateway/projects                     |  create project from JSON   |
| GET    | /gateway/projects/{projectId}         |  get project by ID          |
| GET    | /gateway/projects/status/{status}     |  get project by status      |

Following data can be used for test
- Freelancer ID: 123456,234567,345678,456789
- Projecgt ID: 1,2,3,4,5
- Project status: open,completed,cancelled

## Test on OpenShift

Following URLs are available for test API Gateway Service on OpenShift

```
curl http://apigateway-service-homework.apps.rhpds311.openshift.opentlc.com/gateway/freelancers
curl http://apigateway-service-homework.apps.rhpds311.openshift.opentlc.com/gateway/freelancers/{freelancerId}
curl http://apigateway-service-homework.apps.rhpds311.openshift.opentlc.com/gateway/projects
curl http://apigateway-service-homework.apps.rhpds311.openshift.opentlc.com/gateway/projects/{projectId}
curl http://apigateway-service-homework.apps.rhpds311.openshift.opentlc.com/gateway/projects/status/{status}

```


## Authors

* **Voravit** 
