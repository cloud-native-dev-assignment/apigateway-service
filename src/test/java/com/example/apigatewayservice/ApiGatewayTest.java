package com.example.apigatewayservice;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import com.example.apigatewayservice.model.Freelancer;
import com.example.apigatewayservice.model.Project;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.restassured.http.ContentType;
import wiremock.com.fasterxml.jackson.databind.ObjectMapper;
@RunWith(Arquillian.class)
public class ApiGatewayTest {
    private static int freelancer_port = 8081;
    private static int project_port = 8082;
    
    private Client client;

    @Rule
    public WireMockRule freelancerServiceMock = new WireMockRule(
                            WireMockConfiguration
                            .wireMockConfig().port(freelancer_port));

    @Rule
    public WireMockRule projectServiceMock = new WireMockRule(
                            WireMockConfiguration
                            .wireMockConfig().port(project_port));


    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, "com.example.apigatewayservice")
                .addAsResource("project-local.yml","project-defaults.yml");
    }

    @Before
    public void before() throws Exception {
        client = ClientBuilder.newClient();
    }

    @After
    public void after() throws Exception {
        client.close();
    }

    @Test
    @RunAsClient
    public void testHealthCheck() throws Exception {
        given()
            .get("/health")
            .then()
            .assertThat()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .body("outcome", equalTo("UP"));
    }
    @Test
    @RunAsClient
    public void testGetFreelancerById() throws Exception {
        Freelancer freelancer = new Freelancer();
        freelancer.setId(123456L);
        freelancer.setEmail("jteller@sons.com");
        ObjectMapper mapper = new ObjectMapper();
        String freelancerResponseString = mapper.writeValueAsString(freelancer);
        freelancerServiceMock.stubFor(get(urlMatching("/freelancers/123456")).willReturn(aResponse()
        .withStatus(200).withHeader("Content-Type", "application/json")
        .withBody(freelancerResponseString)));
         given()
            .get("/gateway/freelancers/123456")
            .then()
            .assertThat()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .body("freelancerId",equalTo(123456));
    }

    @Test
    @RunAsClient
    public void testGetFreelancerByIdFailed() throws Exception {
        given()
            .get("/gateway/freelancers/{id}", "1")
            .then()
            .assertThat()
            .statusCode(500);
    }

    @Test
    @RunAsClient
    public void testGetProjectById() throws Exception {
        Project project = new Project();
        project.setProjectId("1");
        project.setProjectName("project1");
        ObjectMapper mapper = new ObjectMapper();
        String projectResponseString = mapper.writeValueAsString(project);
        projectServiceMock.stubFor(get(urlMatching("/projects/1")).willReturn(aResponse()
        .withStatus(200).withHeader("Content-Type", "application/json")
        .withBody(projectResponseString)));
        given()
            .get("/gateway/projects/1")
            .then()
            .assertThat()
            .statusCode(200)
            .body("projectName",equalTo("project1"));
    }

    @Test
    @RunAsClient
    public void testGetProjectByIdFailed() throws Exception {
        given()
            .get("/gateway/projects/{id}", "-1")
            .then()
            .assertThat()
            .statusCode(500);
    }

    
    
}