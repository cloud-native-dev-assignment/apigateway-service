package com.example.apigatewayservice.model;

import java.io.Serializable;

import java.util.Collection;
import java.util.LinkedHashSet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({ "freelancerId", "firstName", "lastName", "email" })
public class Freelancer implements Serializable {
	private static final long serialVersionUID = -635025978137518898L;
    

    @JsonProperty("freelancerId")
    private Long freelancerId;
    

    @JsonProperty("firstName")
    private String firstName;
    

    @JsonProperty("lastName")
    private String lastName;
    

    @JsonProperty("email")
    private String email;
    
 
    private Collection<Skills> skills = new LinkedHashSet<Skills>();

    public Collection<Skills> getSkills(){
        return skills;
    }
    public void setSkills(Collection<Skills> skills){
        this.skills = skills;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    } 
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId(){
        return freelancerId;
    }
    public void setId(Long id){
        this.freelancerId = id;
    }
}