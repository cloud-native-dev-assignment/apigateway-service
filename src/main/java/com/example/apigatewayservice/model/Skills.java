package com.example.apigatewayservice.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({ "skill", "detail" })
public class Skills implements Serializable{

    private static final long serialVersionUID = -182421598881384687L;


    @JsonIgnore
    private Long id;

    @JsonProperty("skill")
    private String skill;

    @JsonProperty("detail")
    private String detail;



    public String getDetail(){
        return detail;
    }
    public void setDetail(String detail){
        this.detail = detail;
    }
   

    public Long getId() {
        return id;
    }
    public String getSkill() {
        return skill;
    }
    public void setSkill(String skill) {
        this.skill = skill;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    
}