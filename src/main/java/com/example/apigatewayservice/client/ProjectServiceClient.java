package com.example.apigatewayservice.client;

import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import com.example.apigatewayservice.model.Project;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

@Path("/projects")
@Retry(maxRetries = 3, maxDuration = 8000)
@Timeout(8000)
public interface ProjectServiceClient {
    @GET
    @Path("/")
    @Fallback(ProjectListFallbackHandler.class)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Project> getAll();

    @POST
    @Path("/")
    // @Fallback(FreelancerListFallbackHandler.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProject(Project project);

    @GET
    @Path("/{projectId}")
    @Fallback(ProjectFallbackHandler.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Project getByProjectId(@PathParam("projectId") String projectId);

    @GET
    @Path("/status/{projectStatus}")
    @Fallback(ProjectListFallbackHandler.class)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Project> getByProjectStatus(@PathParam("projectStatus") String projectStatus);
    
}