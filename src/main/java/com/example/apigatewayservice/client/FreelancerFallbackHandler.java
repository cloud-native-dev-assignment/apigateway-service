package com.example.apigatewayservice.client;

import com.example.apigatewayservice.model.Freelancer;

import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

public class FreelancerFallbackHandler implements FallbackHandler<Freelancer> {

    @Override
    public Freelancer handle(ExecutionContext context) {
        return null;
    }

    
}