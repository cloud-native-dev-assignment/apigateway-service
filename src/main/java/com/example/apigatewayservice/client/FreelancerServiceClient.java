package com.example.apigatewayservice.client;

import com.example.apigatewayservice.model.Freelancer;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@Path("/freelancers")
@Retry(maxRetries = 3,maxDuration= 8000)
@Timeout(8000)
public interface FreelancerServiceClient {
    
    @GET
    @Path("/")
    @Fallback(FreelancerListFallbackHandler.class)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Freelancer> getAll();

    @GET  
    @Path("/{freelancerId}")
    @Fallback(FreelancerFallbackHandler.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Freelancer getById( @PathParam("freelancerId") Long freelancerId);

    
}