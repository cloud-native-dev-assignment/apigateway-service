package com.example.apigatewayservice.client;

import com.example.apigatewayservice.model.Project;

import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

public class ProjectFallbackHandler implements FallbackHandler<Project> {

    @Override
    public Project handle(ExecutionContext context) {
        return null;
    }

    
}