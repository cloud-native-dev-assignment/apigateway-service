package com.example.apigatewayservice.client;

import java.util.List;

import com.example.apigatewayservice.model.Freelancer;

import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

public class FreelancerListFallbackHandler implements FallbackHandler<List<Freelancer>> {

    @Override
    public List<Freelancer> handle(ExecutionContext context) {
        
        return null;
    }

    
}