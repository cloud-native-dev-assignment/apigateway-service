package com.example.apigatewayservice.client;

import java.util.List;

import com.example.apigatewayservice.model.Project;

import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

public class ProjectListFallbackHandler implements FallbackHandler<List<Project>> {

    @Override
    public List<Project> handle(ExecutionContext context) {
        
        return null;
    }

    
}