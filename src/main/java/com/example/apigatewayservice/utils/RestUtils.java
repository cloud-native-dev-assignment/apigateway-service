package com.example.apigatewayservice.utils;

import javax.ws.rs.core.Response;

public class RestUtils {
    public static Response responseInternalServerError(String msg){
			return Response
						.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(msg).build();
	}
    
}