package com.example.apigatewayservice.health;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

@ApplicationScoped
@Health
public class HealthCheckService implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder serverState = HealthCheckResponse.named("server-state");
        return serverState.up().build();
    }
}
