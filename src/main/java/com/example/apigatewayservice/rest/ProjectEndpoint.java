package com.example.apigatewayservice.rest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.apigatewayservice.client.ProjectServiceClient;
import com.example.apigatewayservice.model.Project;
import com.example.apigatewayservice.utils.RestUtils;

import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.jboss.logging.Logger;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

@Path("/projects")
public class ProjectEndpoint {
    private static Logger logger = Logger.getLogger(ProjectEndpoint.class);

    @Inject
    @ConfigurationValue("swarm.project.url")
    private String projectSvc;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        URL apiUrl;

        logger.info("project-service:" + projectSvc);
        try {
            apiUrl = new URL(projectSvc);
            ProjectServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
                    .build(ProjectServiceClient.class);
            List<Project> projects = client.getAll();
            if( projects != null)
				return Response.ok(projects)
					.status(Response.Status.OK).build();
			else
				return RestUtils.responseInternalServerError("Backend Service Error:"+projectSvc);
            
        } catch (MalformedURLException e) {
            return RestUtils.responseInternalServerError(e.getMessage());
        }
    }

    @GET
	@Path("/{projectId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("projectId") String projectId) {
		try {
			URL apiUrl = new URL(projectSvc);
			ProjectServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
					.build(ProjectServiceClient.class);
			Project project = client.getByProjectId(projectId);
			if(project != null)
				return Response.ok(project)
						 .status(Response.Status.OK).build();
			else
				return RestUtils.responseInternalServerError("Backend Service Error:"+projectSvc);
		}  catch (MalformedURLException e) {
			return RestUtils.responseInternalServerError(e.getMessage());
		}
    }
    

    @GET
	@Path("/status/{projectStatus}")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getByProjectStatus(@PathParam("projectStatus") String projectStatus){
        try {
			URL apiUrl = new URL(projectSvc);
			ProjectServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
					.build(ProjectServiceClient.class);
			List<Project> projects = client.getByProjectStatus(projectStatus);
			if(projects != null)
				return Response.ok(projects)
						 .status(Response.Status.OK).build();
			else
				return RestUtils.responseInternalServerError("Backend Service Error:"+projectSvc);
		}  catch (MalformedURLException e) {
			return RestUtils.responseInternalServerError(e.getMessage());
		}
    }
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addProject(Project project){
        try {
			URL apiUrl = new URL(projectSvc);
			ProjectServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
					.build(ProjectServiceClient.class);
			client.addProject(project);
			return Response.ok().status(Response.Status.OK).build();
		
		}  catch (MalformedURLException e) {
			return RestUtils.responseInternalServerError(e.getMessage());
		}
    }

    
    
}