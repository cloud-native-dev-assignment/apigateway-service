package com.example.apigatewayservice.rest;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.apigatewayservice.client.FreelancerServiceClient;
import com.example.apigatewayservice.model.Freelancer;
import com.example.apigatewayservice.utils.RestUtils;

import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.jboss.logging.Logger;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import java.net.MalformedURLException;

import java.net.URL;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@Path("/freelancers")
public class FreelancerEndpoint {

	private static Logger logger = Logger.getLogger(FreelancerEndpoint.class);
	
	@Inject
	@ConfigurationValue("swarm.freelancer.url")
	private String freelancerSvc;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		URL apiUrl;
		try {
			logger.info("freelancer-service:"+freelancerSvc);
			apiUrl = new URL(freelancerSvc);
			FreelancerServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
					.build(FreelancerServiceClient.class);
			List<Freelancer>  freelancers = client.getAll();
			if( freelancers != null)
				return Response.ok(freelancers)
					.status(Response.Status.OK).build();
			else
				return RestUtils.responseInternalServerError("Backend Service Error");
		} catch (MalformedURLException e) {
			return RestUtils.responseInternalServerError(e.getMessage());
		}
		
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") Long id) {
		try {
			logger.info("freelancer-service:"+freelancerSvc);
			URL apiUrl = new URL(freelancerSvc);
			FreelancerServiceClient client = RestClientBuilder.newBuilder().baseUrl(apiUrl)
					.build(FreelancerServiceClient.class);
			Freelancer freelance = client.getById(id);
			if(freelance != null)
				return Response.ok(freelance)
						 .status(Response.Status.OK).build();
			else
				return RestUtils.responseInternalServerError("Backend Service Error");
		}  catch (MalformedURLException e) {
			return RestUtils.responseInternalServerError(e.getMessage());
		}
	}

	
}
