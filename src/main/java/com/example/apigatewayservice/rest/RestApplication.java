package com.example.apigatewayservice.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/gateway")
public class RestApplication extends Application {

}
